module.exports = {
    'parser': '@typescript-eslint/parser',
    'extends': [
        'plugin:react/recommended',
        'plugin:@typescript-eslint/recommended',
        'prettier'
    ],
    'parserOptions': {
        'ecmaVersion': 2018,
        'sourceType': 'module',
        'ecmaFeatures': {
            'jsx': true
        }
    },
    'rules': {
        'prettier/prettier': 0
    },
    'settings': {
        'react': {
            'version': 'detect'
        }
    }
};