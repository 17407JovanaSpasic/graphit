import { TopicDTO } from '../../../DTOs/topic-dto';

export type CreateBlogState = {
    availableTopics: Array<TopicDTO>;
    blogTopics: Array<string>;
};