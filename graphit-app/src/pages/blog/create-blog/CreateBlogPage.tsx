import React from 'react';
import { Component, ReactNode } from 'react';

import { Container, Box, Typography } from '@mui/material';
import { TextField, Select, OutlinedInput, Chip, MenuItem, Button } from '@mui/material';
import { SelectChangeEvent } from '@mui/material/Select';

import { CreateBlogProps } from './create-blog-props';
import { CreateBlogState } from './create-blog-state';

import { TopicDTO } from '../../../DTOs/topic-dto';

export class CreateBlogPage extends Component<CreateBlogProps, CreateBlogState> {
    public state: CreateBlogState = {
        availableTopics: new Array<TopicDTO>(),
        blogTopics: new Array<string>()
    };

    public constructor(props: CreateBlogProps) {
        super(props);

        { /* To-Do: Replace with Axios call. */ }
        this.state.availableTopics.push(new TopicDTO('.NET', '#FF2D34'));
        this.state.availableTopics.push(new TopicDTO('C#', '#2A313C'));
        this.state.availableTopics.push(new TopicDTO('C++', '#954231'));
    }

    private handleChange = (event: SelectChangeEvent<Array<string>>): void =>
        this.setState({ blogTopics: event.target.value as Array<string> });
    
    public render(): ReactNode {
        return (
            <Container style={{ width: '85%' }}>
                <Box sx={{
                    marginTop: 5,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'left'
                }}>
                    <Typography variant='h3' component='h3'>Create Blog</Typography>
                </Box>
                <Box sx={{
                    marginTop: 1,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center'
                }}>
                    <TextField label='Title' margin='dense' fullWidth required />
                    <TextField label='Content' margin='dense' rows={20} fullWidth multiline required />
                    <Box style={{ width: '100%' }}>
                        <Typography component='span'>Choose Topics</Typography>
                        <Select input={<OutlinedInput label='Chip' />} margin='dense' fullWidth multiple
                            value={this.state.blogTopics}
                            renderValue={(selected: Array<string>) => (
                                <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                                    {selected.map((value: string) => (
                                        <Chip key={value} label={value} />
                                    ))}
                                </Box>
                            )}
                            onChange={this.handleChange}
                        >
                            {
                                this.state.availableTopics.map((topic: TopicDTO) => (
                                    <MenuItem key={topic.name} value={topic.name}>
                                        {topic.name}
                                    </MenuItem>
                                ))
                            }
                        </Select>
                    </Box>
                </Box>
                <Box sx={{ display: 'flex', flexWrap: 'wrap' }} style={{ marginTop: 10 }}>
                    <Button className='blog-button' variant='contained'
                        style={{ background: '#DF4551', marginRight: 5 }}
                    >
                        Check text
                    </Button>
                    <Button className='blog-button' variant='contained' disabled>Publish Blog</Button>
                </Box>
            </Container>
        )
    }
}
