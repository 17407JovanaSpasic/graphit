export class TopicDTO {
    private readonly _name: string;
    private readonly _hexColor: string;

    public constructor(name: string, hexColor: string) {
        this._name = name;
        this._hexColor = hexColor;
    }

    public get name(): string { return this._name; }

    public get hexColor(): string { return this._hexColor; }
}