import React, { Component } from "react";
import { blogPreviewProps } from "./blogPreview-props";
import { Typography, Grid, Avatar, Box } from '@mui/material'
import Card from '@mui/material/Card';
import CardActionArea from '@mui/material/CardActionArea';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import { Star } from '@mui/icons-material'


export class BlogPreview extends Component<blogPreviewProps>{

    handleClickUserName: React.MouseEventHandler<HTMLButtonElement> = (e) => {
        console.log(e.currentTarget.value);
    }
    handleClickReadMore: React.MouseEventHandler<HTMLButtonElement> = (e) => {
        console.log(e.currentTarget.value);
    }

    render(): React.ReactNode {
        return (
            <Grid item xs={12} md={20} key={this.props.blogID} style={{ display: 'flex', flexWrap: 'wrap', flexDirection: 'column', maxWidth: "max" }}>
                <CardActionArea component="a" href="#" >
                    <Card >
                        <CardContent sx={{ flex: 1 }}>
                            <Box sx={{ display: 'flex', flexDirection: 'row' }}>
                                <Avatar />
                                <Button variant='text' value={this.props.authorName} onClick={this.handleClickUserName}>
                                {this.props.authorName} {this.props.topAuthor === true?<Star style={{ color: '#FFFF00' }}/>:""} 
                                </Button>
                            </Box>
                            <Typography component="h2" variant="h5" style={{ textAlign: 'center' }}>
                                {this.props.title}
                            </Typography>
                            <Typography variant="subtitle1" paragraph style={{ paddingTop: "10px" }} >
                                {this.props.partOfContent}
                            </Typography>
                            <Button value={this.props.blogID} type='submit' style={{
                                borderRadius: 40,
                                backgroundColor: "#1B90DE"
                            }} key={this.props.blogID} onClick={this.handleClickReadMore} >
                                <Typography color="common.white">Read all</Typography>
                            </Button>
                        </CardContent>

                    </Card>
                </CardActionArea>
            </Grid>

        )
    }

}