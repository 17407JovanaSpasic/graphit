export type blogPreviewProps ={
    blogID: string,
    title: string,
    partOfContent: string,
    authorName: string,
    topAuthor: boolean
}