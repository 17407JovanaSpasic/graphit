import React from 'react'
import { Container, CssBaseline, Typography, Grid, Box } from '@mui/material'
import { UserSubHeader } from './UserSubHeader'
import Button from '@mui/material/Button';
import { Paper } from '@mui/material';
import { BlogPreview } from './BlogPreview';
import { topicsProps } from './topics-props';


export class UserProfile extends React.Component {

    public render(): React.ReactNode {
        const featuredPosts = [
            {
                author: 'Djura',
                title: 'Lorem ipsum',
                description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ullamcorper lacinia mi et malesuada. Mauris lorem ligula, ullamcorper quis nisl a, porta pulvinar ligula. Pellentesque pellentesque nunc ac ipsum blandit, id hendrerit velit suscipit. Donec congue urna in ligula lobortis tempus. Mauris convallis, quam non elementum placerat, enim lorem aliquet lorem, in pulvinar enim metus vitae metus. Donec cursus ex nec turpis rutrum, et congue erat vulputate. Quisque et elit sit amet ante eleifend euismod. Nullam tincidunt aliquet malesuada. Pellentesque vulputate metus eu congue facilisis. Etiam commodo viverra eros. Suspendisse vitae blandit libero. Vestibulum tempor, purus in luctus maximus, velit ligula semper dolor, in faucibus risus neque a massa.',
                image: 'https://source.unsplash.com/random',
                id: '572048240289529520000000000',
                picture: "",
                topAuthor: false
            },
            {
                author: 'Petar',
                title: 'Ipsum lorem',
                description:
                    'Duis est lorem, vestibulum et sapien sit amet, semper venenatis ex. Proin id dui in urna rhoncus lacinia. Phasellus suscipit ipsum nec neque pharetra, ut scelerisque libero lacinia. Maecenas vel magna sodales, suscipit diam non, consectetur felis. Pellentesque odio massa, malesuada a neque sit amet, facilisis eleifend lorem. Nam convallis placerat neque, ac volutpat nulla iaculis sed. ',
                image: 'https://source.unsplash.com/random',
                id: '00000134125000005252310',
                picture: "",
                topAuthor: true

            },
        ];

        const topics: topicsProps[] = [];
        const topic: topicsProps = { topicName: ".NET", hexColor: '#a4724e' }
        topics.push(topic)
        const topic2: topicsProps = { topicName: "Java", hexColor: '#c1cf3e' }
        topics.push(topic2)
        const topic3: topicsProps = { topicName: "Hardware", hexColor: '#06545c' }
        topics.push(topic3)
        const topic4: topicsProps = { topicName: "Software", hexColor: '#a3a5c1' }
        topics.push(topic4)


        return (
            <>
                {/*<Header title='GraphIT' />*/}
                <CssBaseline />
                <Container sx={{ borderBottom: 1, borderColor: 'divider', display: 'flex', flexWrap: 'wrap' }}>
                    <UserSubHeader userName='Dulleh' picture='' name='dule' lastName='sotirov' numberOfFollowers={24} email='dulleh@elfak' topAuthor={true} />
                </Container>

                <Container style={{ display: 'flex', flexWrap: 'wrap', flexDirection: 'column', paddingTop: "30px" }}>
                    <Grid item xs={12} md={20} style={{ padding: "10px" }} >
                        <Paper elevation={0} sx={{ p: 2, bgcolor: 'grey.200' }}>
                            <Typography variant="h6" gutterBottom>
                                Interests
                            </Typography>
                            <Box sx={{ padding: "5px" }}>
                                {topics.map((t) => (
                                    <Button style={{ borderRadius: 40, backgroundColor: t.hexColor, padding: "2px", margin: "5px" }} key={t.topicName}>
                                        <Typography color="common.white">{t.topicName}</Typography>
                                    </Button>
                                ))}
                            </Box>
                        </Paper>
                    </Grid>

                    <Grid container spacing={4} sx={{ display: 'flex', flexDirection: 'column', flexWrap: 'wrap' }} maxWidth="max" >
                        {featuredPosts.map((post) => (
                            <BlogPreview blogID={post.id} title={post.title} partOfContent={post.description} authorName={post.author} key={post.id} topAuthor={post.topAuthor}/>
                        ))}
                    </Grid>
                </Container>
            </>
        )
    }

}