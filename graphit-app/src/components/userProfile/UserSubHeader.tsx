import React from 'react'
import { Avatar, Box, Typography } from '@mui/material'

import { Star } from '@mui/icons-material'
import { userSubHeaderProp } from './userSubHeader-props'

export class UserSubHeader extends React.Component<userSubHeaderProp> {

    render(): React.ReactNode {
        return (
            <Box component="nav" sx={{ justifyContent: 'flex-start', overflowX: 'auto', display: 'flex', flexWrap: 'wrap', flexDirection: 'row' }}>
                <Box sx={{ justifyContent: 'flex-start', overflowX: 'auto', display: 'flex', flexDirection: 'column' }}>
                    <Typography variant='h6'>
                        {this.props.userName}
                        {this.props.topAuthor === true?<Star style={{ color: '#FFFF00' }}/>:""}
                    </Typography>

                    <Avatar alt={this.props.name + " " + this.props.lastName} sx={{ width: 55, height: 55, marginRight: '20px' }} />
                </Box>
                <Box sx={{ justifyContent: 'flex-start', overflowX: 'auto', display: 'flex', flexDirection: 'column', marginTop: '25px' }}>
                    <Typography>Followers {this.props.numberOfFollowers}</Typography>
                    <Typography>{this.props.name + " " + this.props.lastName}</Typography>
                    <Typography>{this.props.email}</Typography>
                </Box >

            </Box>

        )
    }
}

