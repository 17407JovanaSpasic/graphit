export type userSubHeaderProp ={
    userName: string,
    picture: string,
    numberOfFollowers: number,
    name: string,
    lastName: string,
    email:string,
    topAuthor: boolean
}