import React, { Component } from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Divider from '@mui/material/Divider';
import { Link} from 'react-router-dom'
import axios from 'axios';


//type MyProps = any;
type MyState = {
    userName: string,
    password: string,
    token: string,
    logged: boolean
};

type user = {
    name: string,
    lastname:string,
    username: string,
    numberOfFollowers: number,
    topAuthor: boolean,
    picture: number | null,
    personalBlog: PersonalBlogs | null,
    topics: Topics | null,
    token: string

}

type PersonalBlogs = unknown;
type Topics = unknown;

export class Welcome extends Component {
    constructor(props: React.Component) {
        super(props);
    };

    state: MyState = {
        userName: "",
        password: "",
        token: "",
        logged: false
        
    }


    buttonHandler = (event: React.MouseEvent<HTMLButtonElement>) => { //Test button
        event.preventDefault();

        console.log(document.cookie);
        axios.get('https://localhost:5001/User/GetUser/' + this.state.userName,
        {
            headers: {
                'Authorization': 'Bearer ' + window.sessionStorage.getItem("key")
            },
        })
        .then(response =>
             console.log(response.data));
        
      };


    handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if(this.state.userName === "" || this.state.password==="")
        {
          
            this.setState({logged: false});
        }
       
            const userInfo = new FormData();
            userInfo.append("UserName", this.state.userName);
            userInfo.append("Password", this.state.password);
            
            axios.post<user>('https://localhost:5001/User/LogIn', userInfo)
            .then(response => 
                {   
                    this.setState({token: response.data.token});
                    window.sessionStorage.setItem("token", this.state.token);
                    console.log(response.data);
                });
                
            
        
    };


    onUserNameChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
        this.setState({ userName: e.currentTarget.value })
    };

    onPasswordChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
        this.setState({ password: e.currentTarget.value })
    };


    public render(): React.ReactNode {
        return (
            <Container component="main">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Typography variant="h2">Welcome to GraphIT</Typography>

                    <Box component="form" onSubmit={this.handleSubmit} noValidate sx={{ textAlign: 'center' }} >
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            id="userName"
                            label="Username"
                            name="username"
                            autoComplete="username"
                            autoFocus
                            onChange={this.onUserNameChange}
                            color={this.state.userName.length ? 'primary' : 'error'}
                        />

                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            id="password"
                            label="Password"
                            name="password"
                            type="password"
                            autoComplete="current-password"
                            value={this.state.password}
                            onChange={this.onPasswordChange}
                            color={this.state.password.length ? 'primary' : 'error'}
                        />

                        <Button
                            type="submit"
                            size="medium"
                            variant="contained"
                            sx={{ mt: 3, mb: 2, }}
                        >
                            Sign In
                        </Button>

                    </Box>
                </Box>

                <Box sx={{
                    marginTop: 4,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center'
                    }}>
                    <Divider variant="fullWidth" flexItem />
                    <Typography variant="h5"> Dont have account? Register now!</Typography>
                    <Link to='/Register' style={{textDecoration: 'none'}}> 
                        <Button
                            type="submit"
                            size="medium"
                            variant="contained"
                            sx={{ mt: 3, mb: 2, textAlign: 'center'}}
                        >
                            Register
                        </Button>

                    </Link>
                    
                </Box>

                <button onClick={this.buttonHandler} name="bt">
                    Clcik me
                </button>

            </Container>

            

        )

    }
}