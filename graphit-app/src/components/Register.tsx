import React from 'react'
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import FormControl from '@mui/material/FormControl';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import Chip from '@mui/material/Chip';
import axios from 'axios';

type Props = any
type State = {
    name: string,
    lastName: string,
    userName: string,
    email: string,
    password: string,
    allPointsOfInterest: PointsOfInterests[],
    selectedTopics: string[]
}

type PointsOfInterests = {
    name: string,
    hexColor: string
}

export class Register extends React.Component<Props, State>{
    constructor(props: string) {
        super(props);
        this.onSelectInterest = this.onSelectInterest.bind(this);
    };
    state: State = {
        name: "",
        lastName: "",
        userName: "",
        email: "",
        password: "",
        allPointsOfInterest: [],
        selectedTopics: []
    };

    componentDidMount() {
        axios.get<PointsOfInterests[]>(`https://localhost:5001/Topic/GetTopics`)
            .then(res => {
                this.setState({ allPointsOfInterest: res.data });
            })
    }

    handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        const data = new FormData(e.currentTarget);
        //TODO: add fetch
        console.log(window.sessionStorage.getItem("token"));
        console.log(data.get("name"));
        console.log(data.get("lastname"));
        console.log(data.get("username"));
        console.log(data.get("email"));
        console.log(data.get("password"));
        console.log(this.state.selectedTopics);
    };


    onSelectInterest(e: SelectChangeEvent<string[]>) {
        if (Array.isArray(e.target.value)) {
            this.setState({ selectedTopics: e.target.value });
        }
    }


    firstConfigs: string[][] = [["Name", "name", "name"], ["Last name", "lastname", "lastname"]];
    secondConfigs: string[][] = [["Username", "username", "username", "text"], ["Email", "email", "email", "email"],
    ["Password", "password", "password", "password"]];

    render(): React.ReactNode {
        return (
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Typography component="h1" variant="h5"> Create GraphIT account  </Typography>

                    <Box component="form" noValidate onSubmit={this.handleSubmit} sx={{ mt: 3 }}>
                        <Grid container spacing={2}>
                            {this.firstConfigs.map((conf) => (
                                <Grid item xs={12} sm={6} key={conf[0]}>
                                    <TextField
                                        label={conf[0]}
                                        name={conf[1]}
                                        id={conf[2]}
                                        required
                                        fullWidth
                                        autoFocus={conf[1] == "name" ? true : false}
                                    />
                                </Grid>
                            ))}

                            {this.secondConfigs.map((conf) => (
                                <Grid item xs={12} key={conf[2]}>
                                    <TextField
                                        label={conf[0]}
                                        name={conf[1]}
                                        id={conf[2]}
                                        type={conf[3]}
                                        required
                                        fullWidth
                                    />
                                </Grid>
                            ))}
                        </Grid>


                        <div >
                            <FormControl sx={{ m: 1, width: 400, marginTop: 2 }}  >
                                <InputLabel id="demo-multiple-chip-label" >Selecet your interests</InputLabel>
                                <Select
                                    labelId="demo-multiple-chip-label"
                                    id="demo-multiple-chip"
                                    multiple
                                    value={this.state.selectedTopics}
                                    onChange={this.onSelectInterest}
                                    input={<OutlinedInput id="select-multiple-chip" label="Selecet your interests" />}
                                    renderValue={(selected) => (
                                        <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                                            {selected.map((value) => (
                                                <Chip key={value} label={value} />
                                            ))}
                                        </Box>
                                    )}
                                >
                                    {this.state.allPointsOfInterest.map((topic) => (
                                        <MenuItem
                                            key={topic.name}
                                            value={topic.name}
                                        >
                                            <Typography component="h1" variant="subtitle1"> {topic.name}</Typography>
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </div>

                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                        >
                            Create
                        </Button>
                    </Box>
                </Box>
            </Container>
        )
    }

}
