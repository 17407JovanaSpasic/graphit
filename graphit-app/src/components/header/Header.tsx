import React from 'react';
import { Component, ReactNode, Fragment } from 'react';
import { Link, Outlet } from 'react-router-dom';

import { Toolbar, Container, Typography, Box, Button, Avatar } from '@mui/material';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import SettingsIcon from '@mui/icons-material/Settings';
import LogoutIcon from '@mui/icons-material/Logout';

import { HeaderProps } from './header-props';

export class Header extends Component<HeaderProps> {
    public render(): ReactNode {
        return (
            <Fragment>
                <Toolbar
                    sx={{
                        background: '#1B90DE',
                        borderBottom: 1,
                        borderColor: 'divider'
                    }}
                >
                    <Container
                        style={{
                            display: 'flex',
                            flex: 1,
                            flexWrap: 'wrap'
                        }}
                    >
                        <Link to='/' style={{ textDecoration: 'none' }}>
                            <Typography
                                align='left'
                                color='#FFFFFF'
                                component='h2'
                                variant='h5'
                                noWrap
                            >
                                {this.props.title}
                            </Typography>
                        </Link>
                        <Box></Box>
                    </Container>
                    <Link to='/CreateBlogPost'>
                        <Button>
                            <AddCircleIcon />
                        </Button>
                    </Link>
                    <Link to='/UserProfile'>
                        <Button>
                            <Avatar alt='userPicture'
                                sx={{ height: 36, width: 36 }} />
                        </Button>
                    </Link>
                    <Link to='/UserSettings'>
                        <Button>
                            <SettingsIcon />
                        </Button>
                    </Link>
                    <Link to='/Logout'>
                        <Button>
                            <LogoutIcon />
                        </Button>
                    </Link>
                </Toolbar>

                <Outlet />
            </Fragment>
        )
    }
}