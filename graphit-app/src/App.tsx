import React from 'react'
import { Component, ReactNode } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import './App.css';

import { Welcome } from './components/WelcomePage';
import { Register } from './components/Register';

import { Header } from './components/header/Header';

import { CreateBlogPage } from './pages/blog/create-blog/CreateBlogPage';

import { UserProfile } from './components/userProfile/UserProfile';

export class App extends Component {
  public render(): ReactNode {
    const isUserLoggedIn: boolean = sessionStorage.getItem('key') !== null;
    const username = 'TestUser';

    if (!isUserLoggedIn) {
      return (
        <BrowserRouter>
          <Routes>
            <Route path='/' element={<Welcome />} />
            <Route path='Register' element={<Register />} />
          </Routes>
        </BrowserRouter>
      )
    } else {
      return (
        <BrowserRouter>
          <Routes>
            <Route path='/' element={<Header title='GraphIT' />}>
              <Route path='CreateBlogPost' element={<CreateBlogPage username={username} />} />
              <Route path='UserProfile' element={<UserProfile />} />
            </Route>
          </Routes>
        </BrowserRouter>
      )
    }
  }
}
