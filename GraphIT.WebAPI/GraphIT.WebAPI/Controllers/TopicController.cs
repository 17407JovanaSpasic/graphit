﻿using System;
using System.Threading.Tasks;

using GraphIT.Neo4j.DTOs;
using GraphIT.Neo4j.Services.Topic;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Neo4jClient;

namespace GraphIT.WebAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TopicController : ControllerBase
    {
        #region Field(s)
        private readonly ITopicService topicService;
        #endregion Field(s)

        #region Constructor(s)
        public TopicController(IGraphClient client)
        {
            this.topicService = new TopicService(client);
        }
        #endregion Constructor(s)

        #region Method(s)
        [HttpPost]
        [Route("CreateTopic")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateTopic([FromBody] TopicDTO topicDTO)
        {
            try
            {
                await this.topicService.CreateTopicAsync(topicDTO);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetTopics")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetTopics()
        {
            try
            {
                return new JsonResult(await this.topicService.GetTopicsAsync());
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }
        #endregion Method(s)
    }
}
