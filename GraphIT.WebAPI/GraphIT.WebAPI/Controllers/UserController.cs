﻿using GraphIT.Neo4j.DTOs.User;
using GraphIT.Neo4j.Services.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Neo4jClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using GraphIT.Redis.Services.User;
using GraphIT.WebAPI.Token;

namespace GraphIT.WebAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        #region Field

        private readonly IUserServices userServices;

        private readonly IUserServicesRedis userServicesRedis;

        private readonly IReadLaterBlogsServices userServicesRedisReadLaterBlogs;

        #endregion Field

        public UserController(IGraphClient client)
        {
            this.userServices = new UserServices(client);

            this.userServicesRedis = new Redis.Services.User.UserServicesRedis();

            this.userServicesRedisReadLaterBlogs = new Redis.Services.User.ReadLaterBlogsServices();
        }

        #region Create

        [HttpPost]
        [Route("LogIn")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> LogIn([FromForm] LoginUserDTO userCredentials)
        {
            try
            {
                var user = await this.userServices.LogIn(userCredentials);
                if (user.UserName == userCredentials.UserName)
                {
                    var token = JWToken.GenerateToken(user.UserName);
                    user.Token = token;
                    return new JsonResult(user);
                }

                return new UnauthorizedResult();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }

        }


        [HttpPost]
        [Route("CreateUser")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateUser([FromBody] UserDTO userDTO)
        {
            try
            {
                bool operationResult = await this.userServices.CreateUser(userDTO);
                if (operationResult)
                    return Ok();
                else
                    return BadRequest();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
        }

        [HttpPost]
        [Route("AddPointsOfInterests/{userName}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> AddPointsOfInterest([FromBody] List<string> topics, string userName)
        {
            try
            {
                bool operationResult = await this.userServices.AddPointsOfInterest(userName, topics);
                if (operationResult)
                    return Ok();
                else
                    return BadRequest();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Unauthorized();
            }
        }

        [HttpPost]
        [Route("CreateConnectionBetweenUsers/{userName}/{userNameForFollowing}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> CreateConnectionBetweenUsers(string userName, string userNameForFollowing)
        {
            try
            {
                bool operationResult = await this.userServices.CreateRelationshipUserFollowsUser(userName, userNameForFollowing);
                if (operationResult)
                    return Ok();
                else
                    return Unauthorized(); ;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Unauthorized();
            }
        }

        [HttpPost]
        [Route("AddBlogToReadLater/{userName}/{blogId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> AddBlogToReadLater(string userName, string blogId)
        {
            try
            {
                Guid BlogGuidID = Guid.Parse(blogId);
                bool operationResult = await this.userServices.AddReadLaterBlog(userName, BlogGuidID);
                if (operationResult)
                    return Ok();
                else
                    return BadRequest();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Unauthorized();
            }
        }

        
        #endregion Create

        #region Read

        [HttpGet]
        [Route("LogOut/{userName}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public ActionResult LogOut(string userName)
        {
            try
            {
                //TODO: Delete later
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Unauthorized();
            }
        }

        [HttpGet]
        [Route("GetReadLaterBlogs/{userName}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult GetReadLaterBlogs(string userName)
        {
            try
            {
                return new JsonResult(this.userServicesRedisReadLaterBlogs.GetBlogsForReadLater(userName));
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Unauthorized();
            }
        }

        [HttpGet]
        [Route("GetUser/{userName}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> GetUser(string userName)
        {
            var user = await this.userServices.GetUser(userName);
            return new JsonResult(user);  
        }

        [HttpGet]
        [Route("GetTopAuthors")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult GetTopAuthors()
        {
            return new JsonResult(this.userServicesRedis.GetTopAuthors());
        }

        #endregion Read

        #region Update

        [HttpPut]
        [Route("UpdateUser")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult> UpdateUser([FromBody] UserUpdateDTO user)
        {
            var operationResult = await this.userServices.UpdateUsersInfo(user);
            if(operationResult == true)
                return Ok();
            else
                return BadRequest();
        }

        [HttpPut]
        [Route("AddOrUpdateImage")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> AddOrUpdateImage([FromForm] ImageDTO image)
        {
            try
            {
                bool operationResult = await this.userServices.AddOrUpdateImage(image);
                if (operationResult)
                    return Ok();
                else
                    return BadRequest();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Unauthorized();
            }
        }

        #endregion Update

        #region Delete

        [HttpDelete]
        [Route("DeleteUser/{userName}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> DeleteUser(string userName)
        {
            try 
            {
                bool operationResult = await this.userServices.DeleteUser(userName);
                if (operationResult)
                {
                    HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme).Wait();
                    return Ok();
                }
                else
                    return BadRequest();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Unauthorized();
            }
        }

        [HttpDelete]
        [Route("DeletePointsOfInterests/{userName}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> DeletePointsOfInterests([FromBody] List<string> topics, string userName)
        {
            try
            {
                bool operationResult = await this.userServices.RemovePointsOfInterest(userName, topics);
                if (operationResult)
                    return Ok();
                else
                    return BadRequest();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Unauthorized();
            }
        }


        [HttpDelete]
        [Route("DeleteBlogForReadLater/{userName}/{blogId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> DeleteBlogForReadLater(string userName, Guid blogId)
        {
            try
            {
                bool operationResult = await this.userServices.RemoveBlogForReadLater(userName, blogId);
                if (operationResult)
                    return Ok();
                else
                    return BadRequest();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Unauthorized();
            }
        }

        #endregion Delete

    }
}
