﻿using System;
using System.Collections.Generic;

using GraphIT.Redis.Services.ContentFilter;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GraphIT.WebAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ContentFilterController : ControllerBase
    {
        #region Field(s)
        private readonly IContentFilterService contentFilterService;
        #endregion Field(s)

        #region Constructor(s)
        public ContentFilterController()
        {
            this.contentFilterService = new ContentFilterService();
        }
        #endregion Constructor(s)

        #region Method(s)
        [HttpPost]
        [Route("AddInappropriateWords")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult AddInappropriateWords([FromBody] IList<string> inappropriateWords)
        {
            try
            {
                this.contentFilterService.AddInappropriateWordsToSet(inappropriateWords);
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }

            return this.Ok();
        }

        [HttpPost]
        [Route("CheckGivenTextForInappropriateWords")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize]
        public IActionResult CheckGivenTextForInappropriateWords([FromBody] string content)
        {
            try
            {
                return this.Ok(this.contentFilterService.IsGivenTextValid(content));
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }
        #endregion Method(s)
    }
}