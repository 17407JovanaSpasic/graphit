﻿using System;
using System.Threading.Tasks;

using GraphIT.Neo4j.DTOs.Blog;
using GraphIT.Neo4j.DTOs.Blog.Comment;
using GraphIT.Neo4j.Services.Blog;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Neo4jClient;

namespace GraphIT.WebAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class BlogController : ControllerBase
    {
        #region Field(s)
        private readonly IBlogService blogService;
        private readonly Redis.Services.Blog.IBlogService redisBlogService;
        #endregion Field(s)

        #region Constructor(s)
        public BlogController(IGraphClient client)
        {
            this.blogService = new BlogService(client);
            this.redisBlogService = new Redis.Services.Blog.BlogService();
        }
        #endregion Constructor(s)

        #region Method(s)
        [HttpPost]
        [Route("CreateBlog/{authorUsername}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize]
        public async Task<IActionResult> CreateBlog(string authorUsername, [FromBody] BasicBlogDTO blogDTO)
        {
            bool actionSucceeded = await this.blogService.CreateBlogAsync(authorUsername, blogDTO);

            if (actionSucceeded)
            {
                return this.Ok();
            }

            return this.BadRequest();
        }

        [HttpGet]
        [Route("GetBlog/{blogId}")]
        [Authorize]
        public async Task<IActionResult> GetBlog(Guid blogId)
        {
            return new JsonResult(await this.blogService.GetSingleBlogAsync(blogId));
        }

        [HttpGet]
        [Route("GetPreviewBlogsByAuthor/{authorUsername}")]
        [Authorize]
        public async Task<IActionResult> GetPreviewBlogsByAuthor(string authorUsername)
        {
            return new JsonResult(await this.blogService.GetPreviewBlogsByAuthorAsync(authorUsername));
        }

        [HttpGet]
        [Route("GetPreviewBlogsByTopic/{topicName}")]
        [Authorize]
        public async Task<IActionResult> GetPreviewBlogsByTopic(string topicName)
        {
            return new JsonResult(await this.blogService.GetPreviewBlogsByTopicAsync(topicName));
        }

        [HttpPut]
        [Route("UpdateBlog/{blogId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize]
        public async Task<IActionResult> UpdateBlog(Guid blogId, [FromBody] BasicBlogDTO blogDTO)
        {
            bool actionSucceeded = await this.blogService.ModifyBlogAsync(blogId, blogDTO);

            if (actionSucceeded)
            {
                return this.Ok();
            }

            return this.BadRequest();
        }

        [HttpDelete]
        [Route("DeleteBlog/{blogId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize]
        public async Task<IActionResult> DeleteBlog(Guid blogId)
        {
            bool actionSucceeded = await this.blogService.DeleteBlogAsync(blogId);

            if (actionSucceeded)
            {
                try { this.redisBlogService.DeleteCommentsFromList(blogId); }
                catch (Exception ex) { return this.BadRequest(ex.Message); }

                return this.Ok();
            }

            return this.BadRequest();
        }

        [HttpPost]
        [Route("CreateComment/{authorUsername}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize]
        public async Task<IActionResult> CreateComment(string authorUsername,
            [FromBody] BasicCommentDTO commentDTO)
        {
            bool actionSucceeded = await this.blogService.CreateCommentAsync(authorUsername, commentDTO);

            if (actionSucceeded)
            {
                return this.Ok();
            }

            return this.BadRequest();
        }

        [HttpGet]
        [Route("GetAllCommentsByBlog/{blogId}")]
        [Authorize]
        public async Task<IActionResult> GetAllCommentsByBlog(Guid blogId)
        {
            return new JsonResult(await this.blogService.GetAllCommentsByBlogAsync(blogId));
        }

        [HttpGet]
        [Route("GetCachedCommentsByBlog/{blogId}")]
        [Authorize]
        public IActionResult GetCachedCommentsByBlog(Guid blogId)
        {
            return new JsonResult(this.redisBlogService.GetLastNComments(blogId));
        }

        [HttpGet]
        [Route("GetCommentsByBlog/{blogId}/{noCurrentlyVisibleComments}")]
        [Authorize]
        public async Task<IActionResult> GetCommentsByBlog(Guid blogId, int noCurrentlyVisibleComments)
        {
            return new JsonResult(
                await this.blogService.GetCommentsByBlogAsync(blogId, noCurrentlyVisibleComments));
        }

        [HttpPut]
        [Route("UpdateComment/{blogId}/{commentId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize]
        public async Task<IActionResult> UpdateComment(Guid blogId, Guid commentId,
            [FromBody] string commentContent)
        {
            // blogId is used only to find right list in Redis.
            bool actionSucceeded = await this.blogService.ModifyCommentAsync(commentId, commentContent);

            if (actionSucceeded)
            {
                try { this.redisBlogService.CheckAndModifyComment(blogId, commentId, commentContent); }
                catch (Exception ex) { return this.BadRequest(ex.Message); }

                return this.Ok();
            }

            return this.BadRequest();
        }

        [HttpDelete]
        [Route("DeleteComment/{blogId}/{commentId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize]
        public async Task<IActionResult> DeleteComment(Guid blogId, Guid commentId)
        {
            // blogId is used only to find right list in Redis.
            bool actionSucceeded = await this.blogService.DeleteCommentAsync(commentId);

            if (actionSucceeded)
            {
                try { this.redisBlogService.CheckAndDeleteComment(blogId, commentId); }
                catch (Exception ex) { return this.BadRequest(ex.Message); }

                return this.Ok();
            }

            return this.BadRequest();
        }
        #endregion Method(s)
    }
}
