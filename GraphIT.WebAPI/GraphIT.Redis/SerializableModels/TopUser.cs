﻿using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GraphIT.Redis.SerializableModels
{
    [DataContract(Name = "TopUser", Namespace = "http://www.graphit.com")]
    public class TopUser
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public int NumberOfFollowers { get; set; }

        [DataMember]
        public byte[] Picture { get; set; }

        [DataMember]
        public IList<Topic> PointsOfInterest { get; set; }

        public TopUser(string name, string lastName, string userName, int numberOfFollowers, byte[] picture, List<Topic> pointsOfIntereset)
        {
            this.Name = name;
            this.LastName = lastName;
            this.UserName = userName;
            this.NumberOfFollowers = numberOfFollowers;
            this.Picture = picture;
            this.PointsOfInterest = pointsOfIntereset;
        }

        public string ToJsonString()
        {
            return JsonSerializer.SerializeToString<TopUser>(this);
        }

    }
}
