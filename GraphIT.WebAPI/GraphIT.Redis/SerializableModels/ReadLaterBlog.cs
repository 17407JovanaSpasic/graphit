﻿using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GraphIT.Redis.SerializableModels
{
    public class ReadLaterBlog
    {
        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public bool TopAuthor { get; set; }

        [DataMember]
        public string PictureFilePath { get; set; }

        [DataMember]
        public Guid BlogId{ get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string ContentPreview { get; set; }

        public ReadLaterBlog(string userName,bool topAuthor, string pictureFilePath, Guid blogID, string title, string ContentPreview)
        {
            this.UserName = userName;
            this.TopAuthor = topAuthor;
            this.PictureFilePath = pictureFilePath;
            this.BlogId = blogID;
            this.Title = title;
            this.ContentPreview = ContentPreview;
        }

        public string ToJsonString()
        {
            return JsonSerializer.SerializeToString<ReadLaterBlog>(this);
        }
    }
}
