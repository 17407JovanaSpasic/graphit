﻿using System;
using System.Runtime.Serialization;

using ServiceStack.Text;

namespace GraphIT.Redis.SerializableModels
{
    [DataContract(Name = "Comment", Namespace = "http://www.graphit.com")]
    public class Comment
    {
        #region Properties
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public DateTime CreatedAt { get; set; }
        [DataMember]
        public string AuthorUsername { get; set; }
        #endregion Properties

        #region Constructor(s)
        public Comment(Guid id, string content, DateTime createdAt, string username)
        {
            this.Id = id;
            this.Content = content;
            this.CreatedAt = createdAt;
            this.AuthorUsername = username;
        }
        #endregion Constructor(s)

        #region Method(s)
        public string ToJSONString() => JsonSerializer.SerializeToString<Comment>(this);
        #endregion Method(s)
    }
}
