﻿using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GraphIT.Redis.SerializableModels
{
    [DataContract(Name = "TopUser", Namespace = "http://www.graphit.com")]
    public class Topic
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string HexColor { get; set; }

        public Topic(string name, string hexColor)
        {
            this.Name = name;
            this.HexColor = hexColor;
        }

        public string ToJsonString()
        {
            return JsonSerializer.SerializeToString<Topic>(this);
        }
    }
}
