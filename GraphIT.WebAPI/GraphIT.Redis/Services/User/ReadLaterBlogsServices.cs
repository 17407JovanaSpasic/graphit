﻿using GraphIT.Redis.SerializableModels;
using ServiceStack;
using ServiceStack.Redis;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphIT.Redis.Services.User
{
    public class ReadLaterBlogsServices : IReadLaterBlogsServices
    {
        #region Field

        private RedisClient client;

        #endregion Field

        #region Constructor

        public ReadLaterBlogsServices()
        {
            this.client = new RedisClient(RedisConfig.SingleHost);
        }

        #endregion Constructor

        public void AddBlogsToReadLaterCache(string userName, List<ReadLaterBlog> blogs)
        {
            string key = "User:" + userName;
            foreach(var blog in blogs)
                this.client.AddItemToSet(key, blog.ToJsonString());
        }


        public void AddBlogToReadLater(string userName, ReadLaterBlog blog)
        {
            string key = "User:" + userName;
            this.client.AddItemToSet(key, blog.ToJsonString());
        }


        public List<ReadLaterBlog> GetBlogsForReadLater(string userName)
        {
            string key = "User:" + userName;
            var blogs = this.client.GetAllItemsFromSet(key);

            List<ReadLaterBlog> blogsDeserialized = new List<ReadLaterBlog>();
            foreach (var blog in blogs)
            {
                blogsDeserialized.Add(JsonSerializer.DeserializeFromString(blog, typeof(ReadLaterBlog)) as ReadLaterBlog);
            }

            return blogsDeserialized;
        }

        public void DeleteReadLaterBlogs(string userName)
        {
            string key = "User:" + userName;
            this.client.Remove(key);
        }

        public int GetSetCount(string userName)
        {
            string key = "User:" + userName;
            return (int)this.client.GetSetCount(key);
        }
    }
}
