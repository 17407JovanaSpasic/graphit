﻿using GraphIT.Redis.SerializableModels;
using ServiceStack.Redis;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphIT.Redis.Services.User
{
    public class UserServicesRedis : IUserServicesRedis
    {
        #region Field

        private RedisClient client;

        #endregion Field

        #region Constructor

        public UserServicesRedis()
        {
            this.client = new RedisClient(RedisConfig.SingleHost);
        }



        #endregion Constructor

        #region Methods

        public void AddTopAuthor(TopUser topAuthor)
        {

            string key = "topUsers:";

            this.client.PushItemToList(key, topAuthor.ToJsonString());

            this.client.TrimList(key, 0, 15);
        }


        public List<TopUser> GetTopAuthors()
        {
            string key = "topUsers:";
            var topUsers = this.client.GetAllItemsFromList(key);

            List<TopUser> listOfTopUsers = new List<TopUser>();
            foreach (var userStr in topUsers)
            {
                listOfTopUsers.Add(JsonSerializer.DeserializeFromString(userStr, typeof(TopUser)) as TopUser);
            }
            return listOfTopUsers;
        }


        public void IncrementAuthorNumberOfFollowers(string userName)
        {
            Tuple<TopUser, int> tuple = this.GetIndexOfAuthorFromList(userName);

            if (tuple.Item1 == null)
                return;

            if (tuple.Item1.UserName != userName)
                return;

            tuple.Item1.NumberOfFollowers++;

            string key = "topUsers:";
            this.client.SetItemInList(key, tuple.Item2, tuple.Item1.ToJsonString());
        }


        public void UpdateAuthorsPicture(string userName, byte[] picture)
        {
            Tuple<TopUser, int> tuple = this.GetIndexOfAuthorFromList(userName);

            if (tuple.Item1 == null)
                return;

            if (tuple.Item1.UserName != userName)
                return;

            var user = new TopUser(tuple.Item1.Name, tuple.Item1.LastName, tuple.Item1.UserName, tuple.Item1.NumberOfFollowers,
                                    picture, (List<Topic>)tuple.Item1.PointsOfInterest);

            string key = "topUsers:";
            this.client.SetItemInList(key, tuple.Item2, user.ToJsonString());
        }


        public void DecrementAuthorNumberOfFollowers(string userName)
        {
            Tuple<TopUser, int> tuple = this.GetIndexOfAuthorFromList(userName);

            if (tuple.Item1 == null)
                return;

            if (tuple.Item1.UserName != userName)
                return;

            tuple.Item1.NumberOfFollowers--;

            string key = "topUsers:";
            this.client.SetItemInList(key, tuple.Item2, tuple.Item1.ToJsonString());
        }


        public void UpdateAuthorPointsOfInterest(string userName, List<Topic> pointsOfInterests)
        {
            Tuple<TopUser, int> tuple = this.GetIndexOfAuthorFromList(userName);

            if (tuple.Item1 == null)
                return;

            if (tuple.Item1.UserName != userName)
                return;

            tuple.Item1.PointsOfInterest = pointsOfInterests;

            string key = "topUsers:";
            this.client.SetItemInList(key, tuple.Item2, tuple.Item1.ToJsonString());
        }


        public void RemoveAuthor(string userName)
        {
            Tuple<TopUser, int> tuple = this.GetIndexOfAuthorFromList(userName);

            if (tuple.Item1 == null)
                return;

            var markForDelete = Guid.NewGuid().ToString();
            string key = "topUsers:";

            this.client.SetItemInList(key, tuple.Item2, markForDelete);
            this.client.RemoveItemFromList(key, markForDelete);
        }


        public void DeleteList()
        {
            string key = "topUsers:";
            this.client.Del(key);
        }


        public long GetLengthOfList()
        {
            string key = "topUsers:";
            return this.client.LLen(key);
        }


        #endregion Methods


        #region FrequentOperation(s)

        private Tuple<TopUser, int> GetIndexOfAuthorFromList(string userName)
        {
            string key = "topUsers:";
            var listOfTopUsers = this.client.GetAllItemsFromList(key);

            int index = 0;
            TopUser topUser = null;
            foreach(var topAuthorString in listOfTopUsers)
            {
                var topAuthorDeserialized = JsonSerializer.DeserializeFromString(topAuthorString, typeof(TopUser)) as TopUser;
                if (topAuthorDeserialized.UserName == userName)
                {
                    topUser = new TopUser(topAuthorDeserialized.Name, topAuthorDeserialized.LastName, topAuthorDeserialized.UserName,
                                          topAuthorDeserialized.NumberOfFollowers, topAuthorDeserialized.Picture, 
                                          (List<Topic>)topAuthorDeserialized.PointsOfInterest);
                    break;
                }

                index++;
            }

            return Tuple.Create(topUser, index);

        }
        #endregion FrequentOperation(s)
    }
}
