﻿using GraphIT.Redis.SerializableModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphIT.Redis.Services.User
{
    public interface IUserServicesRedis
    {
        public void AddTopAuthor(TopUser TopAuthor);
        public List<TopUser> GetTopAuthors();
        public void UpdateAuthorPointsOfInterest(string userName, List<Topic> pointsOfInterests);
        public void UpdateAuthorsPicture(string userName, byte[] picture);
        public void IncrementAuthorNumberOfFollowers(string userName);
        public void DecrementAuthorNumberOfFollowers(string userName);
        public void RemoveAuthor(string userName);
        public void DeleteList();

        public long GetLengthOfList();
    }
}