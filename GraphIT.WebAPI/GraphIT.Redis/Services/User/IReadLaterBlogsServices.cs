﻿using GraphIT.Redis.SerializableModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphIT.Redis.Services.User
{
    public interface IReadLaterBlogsServices
    {
        void AddBlogsToReadLaterCache(string userName, List<ReadLaterBlog> blogs);
        void AddBlogToReadLater(string userName, ReadLaterBlog blog);
        void DeleteReadLaterBlogs(string userName);
        //void UpdateReadLaterBlog(string userName, SerializableModels.ReadLaterBlog blog);
        List<ReadLaterBlog> GetBlogsForReadLater(string userName);
        public int GetSetCount(string userName);
    }
}
