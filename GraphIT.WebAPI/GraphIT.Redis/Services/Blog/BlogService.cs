﻿using System;
using System.Collections.Generic;

using GraphIT.Redis.SerializableModels;

using ServiceStack.Redis;
using ServiceStack.Text;

namespace GraphIT.Redis.Services.Blog
{
    public class BlogService : IBlogService
    {
        #region Field(s)
        private readonly RedisClient client;
        #endregion Field(s)

        #region Constructor(s)
        public BlogService()
        {
            this.client = new RedisClient(RedisConfig.SingleHost);
        }
        #endregion Constructor(s)

        #region Method(s)
        public void AddNewCommentToList(Guid blogId, Comment comment)
        {
            string listId = "blogs:" + blogId + ":comment";

            if (this.client.GetListCount(listId) == 5)
            {
                this.client.RemoveStartFromList(listId);
            }

            this.client.PushItemToList(listId, comment.ToJSONString());
        }

        public IList<Comment> GetLastNComments(Guid blogId)
        {
            string listId = "blogs:" + blogId + ":comment";

            var listOfComments = new List<Comment>((int)this.client.GetListCount(listId));
            foreach (var commentItem in this.client.GetAllItemsFromList(listId))
            {
                listOfComments.Add(JsonSerializer.DeserializeFromString(commentItem,
                    typeof(Comment)) as Comment);
            }

            listOfComments.Reverse();

            return listOfComments;
        }

        public void CheckAndModifyComment(Guid blogId, Guid commentId, string commentContent)
        {
            string listId = "blogs:" + blogId + ":comment";

            int index = 0;
            foreach (var commentItem in this.client.GetAllItemsFromList(listId))
            {
                var comment = JsonSerializer.DeserializeFromString(commentItem, typeof(Comment)) as Comment;

                if (comment.Id == commentId)
                {
                    comment.Content = commentContent;

                    this.client.SetItemInList(listId, index, comment.ToJSONString());

                    break;
                }

                index++;
            }
        }

        public void CheckAndDeleteComment(Guid blogId, Guid commentId)
        {
            string listId = "blogs:" + blogId + ":comment";

            foreach (var commentItem in this.client.GetAllItemsFromList(listId))
            {
                var comment = JsonSerializer.DeserializeFromString(commentItem, typeof(Comment)) as Comment;

                if (comment.Id == commentId)
                {
                    this.client.RemoveItemFromList(listId, commentItem);

                    break;
                }
            }
        }

        public void DeleteCommentsFromList(Guid blogId) =>
            this.client.RemoveAllFromList("blogs:" + blogId + ":comment");
        #endregion Method(s)
    }
}
