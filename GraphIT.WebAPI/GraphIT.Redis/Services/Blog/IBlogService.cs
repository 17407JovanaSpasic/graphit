﻿using System;
using System.Collections.Generic;

using GraphIT.Redis.SerializableModels;

namespace GraphIT.Redis.Services.Blog
{
    public interface IBlogService
    {
        void AddNewCommentToList(Guid blogId, Comment comment);
        IList<Comment> GetLastNComments(Guid blogId);
        void CheckAndModifyComment(Guid blogId, Guid commentId, string commentContent);
        void CheckAndDeleteComment(Guid blogId, Guid commentId);
        void DeleteCommentsFromList(Guid blogId);
    }
}
