﻿using System.Collections.Generic;

using ServiceStack.Redis;

namespace GraphIT.Redis.Services.ContentFilter
{
    public class ContentFilterService : IContentFilterService
    {
        #region Field(s)
        private readonly RedisClient client;
        #endregion Field(s)

        #region Constructor(s)
        public ContentFilterService()
        {
            this.client = new RedisClient(RedisConfig.SingleHost);
        }
        #endregion Constructor(s)

        #region Method(s)
        public void AddInappropriateWordsToSet(IList<string> listOfInappropriateWords)
        {
            string setId = "graphit:" + "inappropriate" + ":words";

            foreach (var word in listOfInappropriateWords)
            {
                this.client.AddItemToSet(setId, word);
            }
        }

        public bool IsGivenTextValid(string text)
        {
            string setId = "graphit:" + "inappropriate" + ":words";

            var inappropriateWords = this.client.GetAllItemsFromSet(setId);
            var wordsFromText = text.Split(new char[] { ' ', '.', ',', '!', '?', ':', ';' });

            foreach (var word in wordsFromText)
            {
                if (inappropriateWords.Contains(word))
                {
                    return false;
                }
            }

            return true;
        }
        #endregion Method(s)
    }
}
