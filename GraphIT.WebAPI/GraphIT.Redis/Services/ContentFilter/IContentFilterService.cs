﻿using System.Collections.Generic;

namespace GraphIT.Redis.Services.ContentFilter
{
    public interface IContentFilterService
    {
        void AddInappropriateWordsToSet(IList<string> listOfInappropriateWords);
        bool IsGivenTextValid(string text);
    }
}
