﻿using ServiceStack.Redis;

namespace GraphIT.Redis
{
    public static class RedisConfig
    {
        #region Field(s)
        public const bool IgnoreLongTests = true;
        public const int RedisPort = 6379;

        public static readonly string[] MasterHosts = new[] { "127.0.0.1" };
        public static readonly string[] SlaveHosts = new[] { "127.0.0.1" };
        #endregion Field(s)

        #region Properties
        public static string SingleHost => "127.0.0.1";
        public static string SingleHostConnectionString => RedisConfig.SingleHost + ":" + RedisConfig.RedisPort;

        public static BasicRedisClientManager BasicClientManager =>
            new(new[] { RedisConfig.SingleHostConnectionString });
        #endregion Properties
    }
}
