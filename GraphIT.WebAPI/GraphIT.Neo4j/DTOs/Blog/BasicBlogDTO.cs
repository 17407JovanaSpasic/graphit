﻿using System.Collections.Generic;

namespace GraphIT.Neo4j.DTOs.Blog
{
    public class BasicBlogDTO
    {
        #region Properties
        public string Title { get; set; }
        public string Content { get; set; }

        public IList<string> RelevantTopics { get; set; }
        #endregion Properties

        #region Constructor(s)
        public BasicBlogDTO()
        {
            this.RelevantTopics = new List<string>();
        }

        public BasicBlogDTO(string title, string content, IList<string> relevantTopics)
        {
            this.Title = title;
            this.Content = content;
            this.RelevantTopics = relevantTopics;
        }
        #endregion Constructor(s)
    }
}
