﻿using System;
using System.Collections.Generic;

using GraphIT.Neo4j.DTOs.Blog.Author;

namespace GraphIT.Neo4j.DTOs.Blog
{
    public class BlogDTO
    {
        #region Properties
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime LastModified { get; set; }

        public BlogAuthorDTO Author { get; set; }
        public IList<TopicDTO> RelevantTopics { get; set; }
        #endregion Properties

        #region Constructor(s)
        public BlogDTO()
        {
            this.Author = new BlogAuthorDTO();
            this.RelevantTopics = new List<TopicDTO>();
        }
        #endregion Constructor(s)
    }
}
