﻿using System;

namespace GraphIT.Neo4j.DTOs.Blog.Comment
{
    public class BasicCommentDTO
    {
        #region Properties
        public Guid BlogId { get; set; }
        public string Content { get; set; }
        #endregion Properties

        #region Constructor(s)
        public BasicCommentDTO() { }
        #endregion Constructor(s)
    }
}
