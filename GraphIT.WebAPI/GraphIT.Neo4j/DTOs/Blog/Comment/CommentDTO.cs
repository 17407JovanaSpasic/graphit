﻿using System;

namespace GraphIT.Neo4j.DTOs.Blog.Comment
{
    public class CommentDTO
    {
        #region Properties
        public Guid Id { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
        public string AuthorUsername { get; set; }
        #endregion Properties

        #region Constructor(s)
        public CommentDTO() { }
        #endregion Constructor(s)
    }
}
