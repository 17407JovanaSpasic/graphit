﻿namespace GraphIT.Neo4j.DTOs.Blog.Author
{
    public class BlogAuthorDTO
    {
        #region Properties
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public bool IsTopAuthor { get; set; }

        // TODO: Add picture of the author.
        #endregion Properties

        #region Constructor(s)
        public BlogAuthorDTO() { }

        public BlogAuthorDTO(string firstName, string lastName, string username, bool isTopAuthor)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Username = username;
            this.IsTopAuthor = isTopAuthor;
        }
        #endregion Constructor(s)
    }
}
