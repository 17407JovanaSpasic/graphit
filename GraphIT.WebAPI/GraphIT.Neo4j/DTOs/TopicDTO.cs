﻿namespace GraphIT.Neo4j.DTOs
{
    public class TopicDTO
    {
        #region Properties
        public string Name { get; set; }
        public string HexColor { get; set; }
        #endregion Properties

        #region Constructor(s)
        public TopicDTO() { }

        public TopicDTO(string name, string hexColor)
        {
            this.Name = name;
            this.HexColor = hexColor;
        }
        #endregion Constructor(s)
    }
}
