﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GraphIT.Neo4j.DTOs.User
{
    public class UserDTO
    {
        #region Properties
        public string Name { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        #endregion Properties

        #region Constructors
        public UserDTO() { }
        public UserDTO(string name, string lastName, string userName, string email, string password)
        {
            this.Name = name;
            this.LastName = lastName;
            this.UserName = userName;
            this.Email = email;
            this.Password = password;
        }
        #endregion Constructors
    }
}
