﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphIT.Neo4j.DTOs.User
{
    public class LoginUserDTO
    {
        #region Properties
        public string UserName { get; set; }

        public string Password { get; set; }

        #endregion Properties

        #region Constructor

        public LoginUserDTO() { }

        public LoginUserDTO(string userName, string password)
        {
            this.UserName = userName;

            this.Password = password;
        }

        #endregion Constructor

    }
}
