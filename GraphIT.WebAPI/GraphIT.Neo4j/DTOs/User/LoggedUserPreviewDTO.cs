﻿using GraphIT.Neo4j.DTOs.Blog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphIT.Neo4j.DTOs.User
{
    public class LoggedUserPreviewDTO
    {
        #region Properties
        public string Name { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public int NumberOfFollowers { get; set; }

        public bool TopAuthor { get; set; }

        public byte[] Picture { get; set; }

        public string Token { get; set; }

        public IList<PreviewBlogDTO> PersonalBlogs { get; set; }

        public IList<TopicDTO> PointsOfInterest { get; set; }

        #endregion Properties

        #region Constructors
        public LoggedUserPreviewDTO() 
        {
            this.PersonalBlogs = new List<PreviewBlogDTO>();

            this.PointsOfInterest = new List<TopicDTO>();
        }
        public LoggedUserPreviewDTO(string name, string lastName, string userName, string email, int numberOfFollowers, bool topAuthor,
                              string PictureFilePath, IList<PreviewBlogDTO> personalBlogs, List<TopicDTO> pointsOfInterest)
        {
            this.Name = name;
            this.LastName = lastName;
            this.UserName = userName;
            this.Email = email;
            this.NumberOfFollowers = numberOfFollowers;
            this.TopAuthor = topAuthor;
            if (PictureFilePath.Length > 0)
                this.Picture = System.IO.File.ReadAllBytes(@PictureFilePath);

            this.PersonalBlogs = personalBlogs;

            this.PointsOfInterest = pointsOfInterest;
        }
        #endregion Constructors
    }
}
