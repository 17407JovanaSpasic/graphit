﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphIT.Neo4j.DTOs.User
{
    public class ImageDTO
    {
        #region Properties

        public string UserName { get; set;}

        public string ImageName { get; set; }

        public IFormFile Image { get; set; }
        #endregion Properties

        #region constructor
        public ImageDTO() { }
        public ImageDTO(string imageName, IFormFile image)
        {
            this.ImageName = imageName;
            this.Image = image;
        }
        #endregion constructor
    }
}
