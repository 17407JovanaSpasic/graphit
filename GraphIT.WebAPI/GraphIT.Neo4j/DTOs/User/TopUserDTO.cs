﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphIT.Neo4j.DTOs.User
{
    public class TopUserDTO
    {
        public string Name { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public int NumberOfFollowers { get; set; }

        public byte[] Picture { get; set; }

        public IList<TopicDTO> PointsOfInterest { get; set; }

        public TopUserDTO(string name, string lastName, string userName, int numberOfFollowers, byte[] picture, List<TopicDTO> pointsOfIntereset)
        {
            this.Name = name;
            this.LastName = lastName;
            this.UserName = userName;
            this.NumberOfFollowers = numberOfFollowers;
            this.Picture = picture;
            this.PointsOfInterest = pointsOfIntereset;
        }
    }
}
