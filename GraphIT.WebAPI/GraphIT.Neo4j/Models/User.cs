﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphIT.Neo4j.Models.Blog;

namespace GraphIT.Neo4j.Models
{
    public class User
    {
        #region Properties
        public string Name { get; set; }

        public string LastName { get; set; }
        
        public string UserName { get; set; }

        public string Email { get; set; }

        public byte[] PasswordSalt { get; set; }

        public byte[] Password { get; set; }

        public string PictureFilePath { get; set; }

        public int NumberOfFollowers { get; set; }

        public bool TopAuthor { get; set; }

        #endregion Properties

        #region Constructor
        public User()
        {
            Guid _guid = Guid.NewGuid();

            this.PasswordSalt = _guid.ToByteArray();
        }
        #endregion Constructor
    }
}
