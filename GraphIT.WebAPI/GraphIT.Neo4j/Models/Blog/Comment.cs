﻿using System;

namespace GraphIT.Neo4j.Models.Blog
{
    public class Comment
    {
        #region Properties
        public Guid Id { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
        #endregion Properties

        #region Constructor(s)
        public Comment()
        {
            this.Id = Guid.NewGuid();
            this.CreatedAt = DateTime.UtcNow;
        }
        #endregion Constructor(s)
    }
}
