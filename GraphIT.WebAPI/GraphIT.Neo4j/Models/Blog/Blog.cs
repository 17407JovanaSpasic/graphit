﻿using System;

namespace GraphIT.Neo4j.Models.Blog
{
    public class Blog
    {
        #region Properties
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime LastModified { get; set; }
        #endregion Properties

        #region Constructor(s)
        public Blog()
        {
            this.Id = Guid.NewGuid();
            this.LastModified = DateTime.UtcNow;
        }
        #endregion Constructor(s)
    }
}
