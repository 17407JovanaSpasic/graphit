﻿namespace GraphIT.Neo4j.Models
{
    public class Topic
    {
        #region Properties
        public string Name { get; set; }
        public string HexColor { get; set; }
        #endregion Properties

        #region Constructor(s)
        public Topic() { }
        #endregion Constructor(s)
    }
}
