﻿using System.Collections.Generic;
using System.Threading.Tasks;

using GraphIT.Neo4j.DTOs;

namespace GraphIT.Neo4j.Services.Topic
{
    public interface ITopicService
    {
        Task CreateTopicAsync(TopicDTO topicDTO);
        Task<IList<TopicDTO>> GetTopicsAsync();
    }
}
