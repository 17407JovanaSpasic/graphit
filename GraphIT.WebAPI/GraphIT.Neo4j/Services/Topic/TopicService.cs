﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using GraphIT.Neo4j.DTOs;

using Neo4jClient;

namespace GraphIT.Neo4j.Services.Topic
{
    public class TopicService : ITopicService
    {
        #region Field(s)
        private readonly IGraphClient client;
        #endregion Field(s)

        #region Constructor(s)
        public TopicService(IGraphClient client)
        {
            this.client = client;
        }
        #endregion Constructor(s)

        #region Method(s)
        public async Task CreateTopicAsync(TopicDTO topicDTO)
        {
            var topic = new Models.Topic()
            {
                Name = topicDTO.Name,
                HexColor = topicDTO.HexColor
            };

            await this.client.Cypher.Create("(topic:Topic $newTopic)")
                .WithParam("newTopic", topic)
                .ExecuteWithoutResultsAsync();
        }

        public async Task<IList<TopicDTO>> GetTopicsAsync()
        {
            return (await this.client.Cypher.Match("(topic:Topic)")
                .Return(topic => topic.As<TopicDTO>())
                .ResultsAsync).ToList();
        }
        #endregion Method(s)
    }
}
