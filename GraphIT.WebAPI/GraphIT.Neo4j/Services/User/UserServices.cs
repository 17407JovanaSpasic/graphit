﻿using GraphIT.Neo4j.DTOs;
using GraphIT.Neo4j.DTOs.User;
using Neo4jClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using GraphIT.Neo4j.Models;
using System.IO;
using GraphIT.Neo4j.Services.Blog;
using GraphIT.Neo4j.DTOs.Blog.Author;

namespace GraphIT.Neo4j.Services.User
{
    public class UserServices : IUserServices
    {
        #region Fields

        private readonly IGraphClient client;

        private readonly IBlogService blogService;

        private readonly Redis.Services.User.IUserServicesRedis redisUserServices;

        private readonly Redis.Services.User.IReadLaterBlogsServices redisReadLaterBlogs;

        #endregion Fields

        #region Constructor

        public UserServices(IGraphClient _client)
        {
            this.client = _client;

            this.blogService = new BlogService(client);

            this.redisUserServices = new Redis.Services.User.UserServicesRedis();

            this.redisReadLaterBlogs = new Redis.Services.User.ReadLaterBlogsServices();
        }



        #endregion Constructor

        #region CreateNode

        public async Task<bool> CreateUser(UserDTO userDTO)
        {
            string where = $"(u.UserName =~ '{userDTO.UserName}' OR u.Email =~ '{userDTO.Email}' )";
            var existingUser = (await this.client.Cypher.Match("(u: User)")
                                               .Where(where)
                                               .Return(u => u.As<Models.User>())
                                               .ResultsAsync).SingleOrDefault();
            if(existingUser == null)
            {
                Models.User user = new Models.User();
                user.Name = userDTO.Name;
                user.LastName = userDTO.LastName;
                user.UserName = userDTO.UserName;
                user.Email = userDTO.Email;
                user.NumberOfFollowers = 0;
                user.TopAuthor = false;
                user.PictureFilePath = String.Empty;
                user.Password = EncryptPassword(userDTO.Password, user.PasswordSalt);
                if (user.Password.Length == 0)
                    return false;

                await this.client.Cypher.Create("(u: User $props)")
                                        .WithParam("props", user)
                                        .ExecuteWithoutResultsAsync();
                return true;
            }
            else
                return false;
        }

        #endregion CreateNode

        #region CreateRelationships

        public async Task<bool> AddReadLaterBlog(string userName, Guid blogID)
        {
            try
            {
                //Creating relationship between user and the blog in which he is interested to read later.
                var user = (await this.client.Cypher.Match("(u: User)", "(b: Blog)")
                                        .Where((Models.User u) => u.UserName == userName)
                                        .AndWhere((Models.Blog.Blog b) => b.Id == blogID)
                                        .Create("(u)-[:SAVED_TO_READ_LATER]->(b)")
                                        .Return((u) => u.As<Models.User>())
                                        .ResultsAsync).FirstOrDefault();

                var blogInfo = await this.blogService.GetSingleBlogAsync(blogID);
                var authorOfBlog = await this.getUser(blogInfo.Author.Username);

                this.redisReadLaterBlogs.AddBlogToReadLater(userName, new Redis.SerializableModels.ReadLaterBlog(authorOfBlog.UserName, authorOfBlog.TopAuthor,
                    authorOfBlog.PictureFilePath, blogInfo.Id, blogInfo.Title, blogInfo.Content.Substring(0, blogInfo.Content.Length / 4)));
                                        
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public async Task<bool> AddPointsOfInterest(string userName, List<string> topicsName)
        {
            try
            {
                List<Redis.SerializableModels.Topic> listOfTopics = new List<Redis.SerializableModels.Topic>(topicsName.Count);
                //Adding points of interest in which user is interest.
                foreach (var topic in topicsName)
                {
                    var topicModel = (await this.client.Cypher.Match("(u: User)", "(t: Topic)")
                                            .Where((Models.User u) => u.UserName == userName)
                                            .AndWhere((Models.Topic t) => t.Name == topic)
                                            .Create("(u)-[:IS_INTERESTED_IN]->(t)")
                                            .Return((t) => t.As<Models.Topic>())
                                            .ResultsAsync)
                                            .FirstOrDefault();

                    listOfTopics.Add(new Redis.SerializableModels.Topic(topicModel.Name, topicModel.HexColor));
                    
                }
                var user = await this.getUser(userName);
                if (user.TopAuthor)
                    this.redisUserServices.UpdateAuthorPointsOfInterest(userName, listOfTopics);

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public async Task<bool> CreateRelationshipUserFollowsUser(string userName, string UserNameForFollowing)
        {
            try
            {
                //Creating relationship between one user and other user in which first user is interested to follows other user.
                await this.client.Cypher.Match("(user: User)", "(userForFollowing: User )")
                                        .Where((Models.User user) => user.UserName == userName)
                                        .AndWhere((Models.User userForFollowing) => userForFollowing.UserName == UserNameForFollowing)
                                        .Create("(user)-[:FOLLOWS]->(userForFollowing)")
                                        .ExecuteWithoutResultsAsync();

                //Getting user which is followed in previos block of code, for incrementing number of followers.
                var userForFollowing = await this.getUser(UserNameForFollowing);

                userForFollowing.NumberOfFollowers++;
                //If followed user reachs the number of followers of 100, he is promoted to Top author.  
                if (userForFollowing.NumberOfFollowers > 99)
                {
                    if(userForFollowing.NumberOfFollowers == 100)
                    {
                        var interests = await this.getUsersInterests(userForFollowing.UserName);
                        List<Redis.SerializableModels.Topic> topicsRedis = new List<Redis.SerializableModels.Topic>(interests.Count);
                        foreach(var topic in interests)
                        {
                            topicsRedis.Add(new Redis.SerializableModels.Topic(topic.Name, topic.HexColor));
                        }
                        byte[] picture;
                        if (userForFollowing.PictureFilePath.Length > 0)
                            picture = System.IO.File.ReadAllBytes(@userForFollowing.PictureFilePath);
                        else
                            picture = new byte[0];

                        this.redisUserServices.AddTopAuthor(new Redis.SerializableModels.TopUser(userForFollowing.Name, userForFollowing.LastName,
                            userForFollowing.UserName, userForFollowing.NumberOfFollowers, picture, topicsRedis));

                    }
                    else
                        this.redisUserServices.IncrementAuthorNumberOfFollowers(UserNameForFollowing);

                    //Adding another laber to user, label: "Top Author"
                    userForFollowing.TopAuthor = true;
                    await this.client.Cypher.Match("(u: User)")
                                            .Where((Models.User u) => u.UserName == UserNameForFollowing)
                                            .Set("u: TopAuthor")
                                            .Set("u.TopAuthor = $value")
                                            .WithParam("value", userForFollowing.TopAuthor)
                                            .ExecuteWithoutResultsAsync();
                }
                await this.client.Cypher.Match("(u: User)")
                                            .Where((Models.User u) => u.UserName == UserNameForFollowing)
                                            .Set("u.NumberOfFollowers = $number")
                                            .WithParam("number", userForFollowing.NumberOfFollowers)
                                            .ExecuteWithoutResultsAsync();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        #endregion CreateRelationships

        #region DeleteNode

        public async Task<bool> DeleteUser(string userName)
        {
            try
            {
                //Getting all users which was followed by the user.
                var usersWhoFollowsDefinedUser = (await this.client.Cypher.OptionalMatch("(userForRemoval: User)-[FOLLOWS]->(users: User)")
                                                                         .Where((Models.User userForRemoval) => userForRemoval.UserName == userName)
                                                                         .Return((users) => users.As<Models.User>())
                                                                         .ResultsAsync).ToList();

                string where = $"(u.UserName =~ '{userName}')";
                var user = (await this.client.Cypher.Match("(u: User)")
                                                    .Where(where)
                                                    .Return(u => u.As<Models.User>())
                                                    .ResultsAsync).FirstOrDefault();
                if (user == null)
                    return false;

                if (user.TopAuthor)
                    this.redisUserServices.RemoveAuthor(user.UserName);

                await this.blogService.DeleteCommentsByAuthorAsync(userName);

                await this.blogService.DeleteBlogsByAuthorAsync(userName);

                //Detaching and removing user from database.
                string match = $"(u:User{{UserName: '{userName}'}})";
                await this.client.Cypher.Match(match)
                                        .DetachDelete("u")
                                        .ExecuteWithoutResultsAsync();

                //After succesful removing of the user, we are looping through all users which he was following,
                //and decrementing number of followers by one.
                if (usersWhoFollowsDefinedUser[0] != null)
                {
                    foreach (var currentUser in usersWhoFollowsDefinedUser)
                    {
                        if (currentUser.NumberOfFollowers > 0)
                            currentUser.NumberOfFollowers--;

                        if(currentUser.NumberOfFollowers < 100)
                        {
                            await this.client.Cypher.Match("(u: User)")
                                                .Where((Models.User u) => u.UserName == currentUser.UserName)
                                                .Set("u.NumberOfFollowers = $number")
                                                .WithParam("number", currentUser.NumberOfFollowers)
                                                .Remove("u:TopAuthor")
                                                .ExecuteWithoutResultsAsync();

                            this.redisUserServices.RemoveAuthor(currentUser.UserName);

                            continue;
                        }

                        await this.client.Cypher.Match("(u: User)")
                                                .Where((Models.User u) => u.UserName == currentUser.UserName)
                                                .Set("u.NumberOfFollowers = $number")
                                                .WithParam("number", currentUser.NumberOfFollowers)
                                                .ExecuteWithoutResultsAsync();

                        this.redisUserServices.DecrementAuthorNumberOfFollowers(currentUser.UserName);

                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }


        #endregion DeleteNode

        #region DeleteRelationships

        public async Task<bool> RemovePointsOfInterest(string userName, List<string> topicsName)
        {
            try
            {
                foreach (var topic in topicsName)
                {
                    await this.client.Cypher.OptionalMatch("(u: User)-[r:IS_INTERESTED_IN]->(t: Topic)")
                                             .Where((Models.User u) => u.UserName == userName)
                                             .AndWhere((Models.Topic t) => t.Name == topic)
                                             .Delete("r")
                                             .ExecuteWithoutResultsAsync();
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public async Task<bool> RemoveBlogForReadLater(string userName, Guid blogID)
        {
            try
            {

                await this.client.Cypher.OptionalMatch("(u: User)-[r:SAVED_TO_READ_LATER]->(b: Blog)")
                                            .Where((Models.User u) => u.UserName == userName)
                                            .AndWhere((Models.Blog.Blog b) => b.Id == blogID)
                                            .Delete("r")
                                            .ExecuteWithoutResultsAsync();
                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        #endregion DeleteRelationships

        #region GetMethods

        public async Task<UserPreviewDTO> GetUser(string userName)
        {
            try
            {
                var user = await this.getUser(userName);
                var blogs = await this.blogService.GetPreviewBlogsByAuthorAsync(userName);
                var interests = await this.getUsersInterests(userName);

                if (user == null)
                    return new UserPreviewDTO();
                
                return new UserPreviewDTO(user.Name, user.LastName,user.UserName, user.Email, user.NumberOfFollowers, 
                                          user.TopAuthor, user.PictureFilePath, blogs, interests);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new UserPreviewDTO();
            }
        }

        public async Task<LoggedUserPreviewDTO> LogIn(LoginUserDTO userCredentials)
        {
            try
            {
                if (string.IsNullOrEmpty(userCredentials.UserName) || string.IsNullOrEmpty(userCredentials.Password))
                    return new LoggedUserPreviewDTO() { UserName = "invalidUser" };

                var user = await this.getUser(userCredentials.UserName);
                if (user == null)
                    return new LoggedUserPreviewDTO();

                byte[] encryptedPassword = this.EncryptPassword(userCredentials.Password, user.PasswordSalt);
                if(encryptedPassword.SequenceEqual(user.Password))
                {
                    var blogs = await this.blogService.GetPreviewBlogsByAuthorAsync(userCredentials.UserName);
                    var interests = await this.getUsersInterests(userCredentials.UserName);

                    var readLaterBlogs = (await this.client.Cypher.Match("(u: User)-[SAVED_TO_READ_LATER]->(b: Blog)")
                                                                   .Where((Models.User u) => u.UserName == userCredentials.UserName)
                                                                   .OptionalMatch("(b)-[:IS_RELEVANT_TO]->(t:Topic)")
                                                                   .OptionalMatch("(author:User)-[:HAS_PUBLISHED]->(b)")
                                                                   .Return((u, b, t, author) => new
                                                                   {
                                                                       user = u.As<Models.User>(),
                                                                       blog = b.As<Models.Blog.Blog>(),
                                                                       topic = t.CollectAs<TopicDTO>(),
                                                                       authorOfBlog = author.As<Models.User>()

                                                                   })
                                                                   .ResultsAsync)
                                                                   .Select(blogRL => new Redis.SerializableModels.ReadLaterBlog(blogRL.authorOfBlog.UserName, blogRL.authorOfBlog.TopAuthor,
                                                                                    blogRL.authorOfBlog.PictureFilePath, blogRL.blog.Id, blogRL.blog.Title,
                                                                                    blogRL.blog.Content.Substring(0, blogRL.blog.Content.Length / 4)))
                                                                   .ToList();

                    if (redisReadLaterBlogs.GetSetCount(userCredentials.UserName) == 0)
                    {
                        //Adding to cache
                        this.redisReadLaterBlogs.AddBlogsToReadLaterCache(userCredentials.UserName, readLaterBlogs);
                    }
                    return new LoggedUserPreviewDTO(user.Name, user.LastName, user.UserName, user.Email, user.NumberOfFollowers,
                                                    user.TopAuthor, user.PictureFilePath, blogs, interests);
                }
                return new LoggedUserPreviewDTO();

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new LoggedUserPreviewDTO();
            }
            
        }

        public async Task GetTopAuthors()
        {
            try
            {
                if (this.redisUserServices.GetLengthOfList() > 0)
                    return;

                List<TopUserDTO> listOfTopAuthors = new List<TopUserDTO>();

                var topAuthors = (await this.client.Cypher.Match("(u: TopAuthor)")
                                               .Return(u => u.As<Models.User>())
                                               .OrderByDescending("u.NumberOfFollowers")
                                               .Limit(15)
                                               .ResultsAsync).ToList();
                if (topAuthors[0] == null)
                    return;

                foreach(var user in topAuthors)
                {
                    var interests = await this.getUsersInterests(user.UserName);
                    byte[] picture;
                    if (user.PictureFilePath.Length == 0)
                        picture = new byte[0];
                    else
                        picture = System.IO.File.ReadAllBytes(@user.PictureFilePath);

                    List<GraphIT.Redis.SerializableModels.Topic> listOfTopicRedis = new List<Redis.SerializableModels.Topic>(interests.Count);
                    foreach (var topic in interests)
                    {
                        listOfTopicRedis.Add(new GraphIT.Redis.SerializableModels.Topic(topic.Name, topic.HexColor));
                    }
                    this.redisUserServices.AddTopAuthor(new GraphIT.Redis.SerializableModels.TopUser(user.Name, user.LastName, user.UserName,
                                                            user.NumberOfFollowers, /*picture*/ null, listOfTopicRedis));
                }

                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
        }

        #endregion GetMethods

        #region UpdateProperties

        public async Task<bool> AddOrUpdateImage(ImageDTO imageDTO)
        {
            try
            {
                var user = await this.getUser(imageDTO.UserName);
                if (user == null)
                    return false;

                if (user.PictureFilePath.Length > 0)
                    File.Delete(user.PictureFilePath);

                string path = this.writeImageInFileSystem(imageDTO.UserName, imageDTO);
                if (path == "error")
                    return false;

                if (await this.setImagePathProperty(imageDTO.UserName, path))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }


        public async Task<bool> UpdateUsersInfo(UserUpdateDTO userDTO)
        {
            try
            {

                var user = await this.getUser(userDTO.UserName);
                if (user == null)
                    return false;
                var password = this.EncryptPassword(userDTO.OldPassword, user.PasswordSalt);
                if ((password.SequenceEqual(user.Password)) == false)
                    return false;

                var newPassword = this.EncryptPassword(userDTO.NewPassword, user.PasswordSalt);

                await this.client.Cypher.Match("(u:User)")
                                        .Where((Models.User u) => u.UserName == userDTO.UserName)
                                        .Set("u.Password = $newPassword")
                                        .WithParam("newPassword", newPassword)
                                        .Set("u.Email = $email")
                                        .WithParam("email", userDTO.Email)
                                        .ExecuteWithoutResultsAsync();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        #endregion UpdateProperties

        #region FrequentQueryOperations

        private async Task<Models.User> getUser(string userName)
        {
            try
            {
                return (await this.client.Cypher.Match("(u: User)")
                                               .Where((Models.User u) => u.UserName == userName)
                                               .Return(u => u.As<Models.User>())
                                               .ResultsAsync).FirstOrDefault();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        private async Task<List<Models.Blog.Blog>> getUsersBlogs(string userName)
        {
            try
            {
                var blogs = (await this.client.Cypher.OptionalMatch("(blogs: Blog)-[HAS_AUTHOR]->(_user: User)")
                                                      .Where((Models.User _user) => _user.UserName == userName)
                                                      .Return((blogs) => blogs.As<Models.Blog.Blog>())
                                                      .ResultsAsync).ToList();
                if (blogs[0] == null)
                    return new List<Models.Blog.Blog>();
                else
                    return blogs;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<Models.Blog.Blog>();
            }
        }

        private async Task<List<TopicDTO>> getUsersInterests(string userName)
        {
            try
            {
                var topics = (await this.client.Cypher.OptionalMatch("(_user: User)-[IS_INTERESTED_IN]->(topics: Topic)")
                                                         .Where((Models.User _user) => _user.UserName == userName)
                                                         .Return((topics) => topics.As<TopicDTO>())
                                                         .ResultsAsync).ToList();
                if (topics[0] == null)
                    return new List<TopicDTO>();
                else
                    return topics;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<TopicDTO>();
            }
        }

        public async Task<bool> setImagePathProperty(string userName, string filePath)
        {
            try
            {
                await this.client.Cypher.Match("(u: User)")
                                  .Where((Models.User u) => u.UserName == userName)
                                  .Set("u.PictureFilePath = $imagePath")
                                  .WithParam("imagePath", filePath)
                                  .ExecuteWithoutResultsAsync();

                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public string writeImageInFileSystem(string userName, ImageDTO imageDTO)
        {
            try
            {
                byte[] imageBinary = null;
                if (imageDTO.Image.Length > 0)
                {
                    using (var binaryReader = new BinaryReader(imageDTO.Image.OpenReadStream()))
                    {
                        imageBinary = binaryReader.ReadBytes((int)imageDTO.Image.Length);
                        this.redisUserServices.UpdateAuthorsPicture(userName, imageBinary);
                    }
                }
                else
                    return "error";

                string path = "C:\\Users\\DusanSotirov\\Pictures\\" + userName + "_" + imageDTO.ImageName;
                File.WriteAllBytes(path, imageBinary);

                return path;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "error";
            }
        }
        #endregion FrequentQueryOperations

        #region EncryptionMethods

        private byte[] EncryptPassword(string password, byte[] passwordSalt)
        {
            byte[] hashedPassword;
            byte[] originalPassword;
            if (string.IsNullOrEmpty(password) || passwordSalt.Length == 0)
                return hashedPassword = new byte[0];

            originalPassword = Encoding.ASCII.GetBytes(password);

            int totalLengthCombined = (originalPassword.Length + passwordSalt.Length);

            hashedPassword = new byte[totalLengthCombined];
            int i;
            for ( i=0 ; i < passwordSalt.Length-1; i++)
            {
                hashedPassword[i] = passwordSalt[i];          
            }
            for(int j = 0; j < originalPassword.Length -1; j++)
            {
                hashedPassword[i] = originalPassword[j];
                i++;
            }
            return HashPassword(hashedPassword);
        }

        private byte[] HashPassword(byte[] password)
        {
            SHA256 _sha256 = SHA256.Create();
            return _sha256.ComputeHash(password);
        }
        #endregion EncryptionMethods
    }
}
