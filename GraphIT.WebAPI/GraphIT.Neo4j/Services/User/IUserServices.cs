﻿using GraphIT.Neo4j.DTOs;
using GraphIT.Neo4j.DTOs.User;
using GraphIT.Neo4j.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace GraphIT.Neo4j.Services.User
{
    public interface IUserServices
    {
        Task<bool> CreateUser(UserDTO userDTO);
        Task<UserPreviewDTO> GetUser(string userName);
        Task<LoggedUserPreviewDTO> LogIn(LoginUserDTO userCredentials);
        Task GetTopAuthors();
        Task<bool> DeleteUser(string userName);
        Task<bool> AddReadLaterBlog(string userName, Guid blogID);
        Task<bool> AddPointsOfInterest(string userName, List<string> topicsName);
        Task<bool> CreateRelationshipUserFollowsUser(string userName, string followedUserName);
        Task<bool> AddOrUpdateImage(ImageDTO imageDTO);
        Task<bool> RemovePointsOfInterest(string userName, List<string> topicsName);
        Task<bool> UpdateUsersInfo(UserUpdateDTO userDTO);
        Task<bool> RemoveBlogForReadLater(string userName, Guid blogID);
    }
}
