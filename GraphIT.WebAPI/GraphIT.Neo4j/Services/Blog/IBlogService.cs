﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using GraphIT.Neo4j.DTOs.Blog;
using GraphIT.Neo4j.DTOs.Blog.Comment;

namespace GraphIT.Neo4j.Services.Blog
{
    public interface IBlogService
    {
        Task<bool> CreateBlogAsync(string username, BasicBlogDTO blogDTO);
        Task<BlogDTO> GetSingleBlogAsync(Guid blogId);
        Task<IList<PreviewBlogDTO>> GetPreviewBlogsByTopicAsync(string topicName);
        Task<IList<PreviewBlogDTO>> GetPreviewBlogsByAuthorAsync(string username);
        Task<bool> ModifyBlogAsync(Guid blogId, BasicBlogDTO blogDTO);
        Task<bool> DeleteBlogAsync(Guid blogId);

        Task<bool> CreateCommentAsync(string username, BasicCommentDTO commentDTO);
        Task<IList<CommentDTO>> GetAllCommentsByBlogAsync(Guid blogId);
        Task<IList<CommentDTO>> GetCommentsByBlogAsync(Guid blogId, int noCurrentlyVisibleComments);
        Task<bool> ModifyCommentAsync(Guid commentId, string commentContent);
        Task<bool> DeleteCommentAsync(Guid commentId);
        
        Task DeleteBlogsByAuthorAsync(string username);
        Task DeleteCommentsByAuthorAsync(string username);
    }
}
