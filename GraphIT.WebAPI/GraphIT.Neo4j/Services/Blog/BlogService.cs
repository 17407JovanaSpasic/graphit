﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using GraphIT.Neo4j.DTOs;
using GraphIT.Neo4j.DTOs.Blog;
using GraphIT.Neo4j.DTOs.Blog.Author;
using GraphIT.Neo4j.DTOs.Blog.Comment;
using GraphIT.Redis.SerializableModels;

using Neo4jClient;

namespace GraphIT.Neo4j.Services.Blog
{
    public class BlogService : IBlogService
    {
        #region Field(s)
        private readonly IGraphClient client;
        private readonly Redis.Services.Blog.IBlogService redisBlogService;
        #endregion Field(s)

        #region Constructor(s)
        public BlogService(IGraphClient client)
        {
            this.client = client;
            this.redisBlogService = new Redis.Services.Blog.BlogService();
        }
        #endregion Constructor(s)

        #region Method(s)
        public async Task<bool> CreateBlogAsync(string username, BasicBlogDTO blogDTO)
        {
            var newBlog = new Models.Blog.Blog()
            {
                Title = blogDTO.Title,
                Content = blogDTO.Content
            };

            try
            {
                // Create new blog and connect it with the author.
                await this.client.Cypher.Match("(author:User)")
                    .Where((Models.User author) => author.UserName == username)
                    .Create("(blog:Blog $newBlog)<-[:HAS_PUBLISHED]-(author)")
                    .WithParam("newBlog", newBlog)
                    .ExecuteWithoutResultsAsync();

                // Connect created blog with relevant topics.
                await this.ConnectBlogWithTopicsAsync(newBlog.Id, blogDTO.RelevantTopics);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }

            return true;
        }

        public async Task<BlogDTO> GetSingleBlogAsync(Guid blogId)
        {
            try
            {
                return (await this.client.Cypher.Match("(author:User)-[:HAS_PUBLISHED]->(blog:Blog)")
                    .Where((Models.Blog.Blog blog) => blog.Id == blogId)
                    .OptionalMatch("(blog)-[:IS_RELEVANT_TO]->(topic:Topic)")
                    .Return((author, blog, topic) => new
                    {
                        Author = author.As<Models.User>(),
                        Blog = blog.As<Models.Blog.Blog>(),
                        Topics = topic.CollectAs<TopicDTO>()
                    }).ResultsAsync)
                    .Select(obj => new BlogDTO
                    {
                        Id = obj.Blog.Id,
                        Title = obj.Blog.Title,
                        Content = obj.Blog.Content,
                        LastModified = obj.Blog.LastModified,
                        Author = new BlogAuthorDTO(obj.Author.Name, obj.Author.LastName,
                            obj.Author.UserName, obj.Author.TopAuthor),
                        RelevantTopics = (obj.Topics != null) ? obj.Topics.ToList() : new List<TopicDTO>()
                    }).SingleOrDefault();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return new BlogDTO();
            }
        }

        public async Task<IList<PreviewBlogDTO>> GetPreviewBlogsByAuthorAsync(string username)
        {
            try
            {
                return (await this.client.Cypher.Match("(author:User)-[:HAS_PUBLISHED]->(blog:Blog)")
                    .Where((Models.User author) => author.UserName == username)
                    .OptionalMatch("(blog)-[:IS_RELEVANT_TO]->(topic:Topic)")
                    .Return((author, blog, topic) => new
                    {
                        Author = author.As<Models.User>(),
                        Blog = blog.As<Models.Blog.Blog>(),
                        Topics = topic.CollectAs<TopicDTO>()
                    }).ResultsAsync)
                    .Select(obj => new PreviewBlogDTO
                    {
                        Id = obj.Blog.Id,
                        Title = obj.Blog.Title,
                        PartOfContent = obj.Blog.Content.Substring(0, obj.Blog.Content.Length / 4),
                        LastModified = obj.Blog.LastModified,
                        Author = new BlogAuthorDTO(obj.Author.Name, obj.Author.LastName,
                            obj.Author.UserName, obj.Author.TopAuthor),
                        RelevantTopics = (obj.Topics != null) ? obj.Topics.ToList() : new List<TopicDTO>()
                    }).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return new List<PreviewBlogDTO>();
            }
        }

        public async Task<IList<PreviewBlogDTO>> GetPreviewBlogsByTopicAsync(string topicName)
        {
            try
            {
                return (await this.client.Cypher
                    .Match("(author:User)-[:HAS_PUBLISHED]->(blog:Blog)-[:IS_RELEVANT_TO]->(topic:Topic)")
                    .Where((Models.Topic topic) => topic.Name == topicName)
                    .Return((author, blog, topic) => new
                    {
                        Author = author.As<Models.User>(),
                        Blog = blog.As<Models.Blog.Blog>(),
                        Topics = topic.CollectAs<TopicDTO>()
                    }).ResultsAsync)
                    .Select(obj => new PreviewBlogDTO
                    {
                        Id = obj.Blog.Id,
                        Title = obj.Blog.Title,
                        PartOfContent = obj.Blog.Content.Substring(0, obj.Blog.Content.Length / 4),
                        LastModified = obj.Blog.LastModified,
                        Author = new BlogAuthorDTO(obj.Author.Name, obj.Author.LastName,
                            obj.Author.UserName, obj.Author.TopAuthor),
                        RelevantTopics = obj.Topics.ToList()
                    }).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return new List<PreviewBlogDTO>();
            }
        }

        public async Task<bool> ModifyBlogAsync(Guid blogId, BasicBlogDTO blogDTO)
        {
            try
            {
                // Get existing blog with relevant topics.
                dynamic blogWithTopicsObj = await this.GetBlogInformation(blogId);

                if (blogWithTopicsObj.Blog is null)
                {
                    return false;
                }

                // Remove old topics, if they exist.
                if (blogWithTopicsObj.Topics != null)
                {
                    var listOfTopicsToBeRemoved = new List<string>();
                    foreach (var topic in blogWithTopicsObj.Topics)
                    {
                        if (!blogDTO.RelevantTopics.Contains(topic.Name))
                        {
                            listOfTopicsToBeRemoved.Add(topic.Name);
                        }
                    }

                    await this.RemoveOldTopicsFromBlogAsync(blogId, listOfTopicsToBeRemoved);
                }

                // Add new topics.
                await this.ConnectBlogWithTopicsAsync(blogId, blogDTO.RelevantTopics);

                await this.client.Cypher.Match("(blog:Blog)")
                    .Where((Models.Blog.Blog blog) => blog.Id == blogId)
                    .Set("blog.Title = $newTitle")
                    .WithParam("newTitle", blogDTO.Title)
                    .Set("blog.Content = $newContent")
                    .WithParam("newContent", blogDTO.Content)
                    .Set("blog.LastModified = $newDate")
                    .WithParam("newDate", DateTime.UtcNow)
                    .ExecuteWithoutResultsAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }

            return true;
        }

        public async Task<bool> DeleteBlogAsync(Guid blogId)
        {
            try
            {
                await this.DeleteCommentsByBlogAsync(blogId);

                await this.client.Cypher.Match("()-[inR]->(blog:Blog)-[outR]->()")
                    .Where((Models.Blog.Blog blog) => blog.Id == blogId)
                    .Delete("inR, outR, blog")
                    .ExecuteWithoutResultsAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }

            return true;
        }

        public async Task<bool> CreateCommentAsync(string username, BasicCommentDTO commentDTO)
        {
            var newComment = new Models.Blog.Comment()
            {
                Content = commentDTO.Content
            };

            try
            {
                await this.client.Cypher.Match("(author:User)", "(blog:Blog)")
                    .Where((Models.User author) => author.UserName == username)
                    .AndWhere((Models.Blog.Blog blog) => blog.Id == commentDTO.BlogId)
                    .Create("(comment:Comment $newComment)-[:IS_WRITTEN_BY]->(author)")
                    .WithParam("newComment", newComment)
                    .Create("(blog)-[:CONTAINS]->(comment)")
                    .ExecuteWithoutResultsAsync();

                this.redisBlogService.AddNewCommentToList(commentDTO.BlogId, new Comment(newComment.Id,
                    newComment.Content, newComment.CreatedAt, username));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }

            return true;
        }

        public async Task<IList<CommentDTO>> GetAllCommentsByBlogAsync(Guid blogId)
        {
            try
            {
                return (await this.client.Cypher.Match("(blog:Blog)-[:CONTAINS]->(comment:Comment)")
                    .Where((Models.Blog.Blog blog) => blog.Id == blogId)
                    .Match("(comment)-[:IS_WRITTEN_BY]->(author:User)")
                    .Return((comment, author) => new
                    {
                        Comment = comment.As<Models.Blog.Comment>(),
                        Author = author.As<Models.User>()
                    }).OrderByDescending("comment.CreatedAt")
                    .ResultsAsync)
                    .Select(obj => new CommentDTO
                    {
                        Id = obj.Comment.Id,
                        Content = obj.Comment.Content,
                        CreatedAt = obj.Comment.CreatedAt,
                        AuthorUsername = obj.Author.UserName
                    }).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return new List<CommentDTO>();
            }
        }

        public async Task<IList<CommentDTO>> GetCommentsByBlogAsync(Guid blogId, int noCurrentlyVisibleComments)
        {
            try
            {
                return (await this.client.Cypher.Match("(blog:Blog)-[:CONTAINS]->(comment:Comment)")
                    .Where((Models.Blog.Blog blog) => blog.Id == blogId)
                    .Match("(comment)-[:IS_WRITTEN_BY]->(author:User)")
                    .Return((comment, author) => new
                    {
                        Comment = comment.As<Models.Blog.Comment>(),
                        Author = author.As<Models.User>()
                    }).OrderByDescending("comment.CreatedAt")
                    .Skip(noCurrentlyVisibleComments)
                    .ResultsAsync)
                    .Select(obj => new CommentDTO
                    {
                        Id = obj.Comment.Id,
                        Content = obj.Comment.Content,
                        CreatedAt = obj.Comment.CreatedAt,
                        AuthorUsername = obj.Author.UserName
                    }).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return new List<CommentDTO>();
            }
        }

        public async Task<bool> ModifyCommentAsync(Guid commentId, string commentContent)
        {
            try
            {
                await this.client.Cypher.Match("(comment:Comment)")
                    .Where((Models.Blog.Comment comment) => comment.Id == commentId)
                    .Set("comment.Content = $newContent")
                    .WithParam("newContent", commentContent)
                    .ExecuteWithoutResultsAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }

            return true;
        }

        public async Task<bool> DeleteCommentAsync(Guid commentId)
        {
            try
            {
                await this.client.Cypher.Match("()-[inR]->(comment:Comment)-[outR]->()")
                    .Where((Models.Blog.Comment comment) => comment.Id == commentId)
                    .Delete("inR, comment, outR")
                    .ExecuteWithoutResultsAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }

            return true;
        }
        #endregion Method(s)

        #region Helper Method(s)
        private async Task ConnectBlogWithTopicsAsync(Guid blogId, IList<string> listOfTopicNames)
        {
            foreach (var topicName in listOfTopicNames)
            {
                await this.client.Cypher.Match("(blog:Blog)", "(topic:Topic)")
                    .Where((Models.Blog.Blog blog) => blog.Id == blogId)
                    .AndWhere((Models.Topic topic) => topic.Name == topicName)
                    .Merge("(blog)-[:IS_RELEVANT_TO]->(topic)")
                    .ExecuteWithoutResultsAsync();
            }
        }

        private async Task<object> GetBlogInformation(Guid blogId) =>
            (await this.client.Cypher.Match("(blog:Blog)")
            .Where((Models.Blog.Blog blog) => blog.Id == blogId)
            .OptionalMatch("(blog)-[:IS_RELEVANT_TO]->(topic:Topic)")
            .Return((blog, topic) => new
            {
                Blog = blog.As<Models.Blog.Blog>(),
                Topics = topic.CollectAs<TopicDTO>()
            }).ResultsAsync)
            .SingleOrDefault();

        private async Task RemoveOldTopicsFromBlogAsync(Guid blogId, IList<string> listOfTopicNames)
        {
            foreach (var topicName in listOfTopicNames)
            {
                await this.client.Cypher.Match("(blog:Blog)-[r:IS_RELEVANT_TO]->(topic:Topic)")
                    .Where((Models.Blog.Blog blog) => blog.Id == blogId)
                    .AndWhere((Models.Topic topic) => topic.Name == topicName)
                    .Delete("r")
                    .ExecuteWithoutResultsAsync();
            }
        }

        private async Task DeleteCommentsByBlogAsync(Guid blogId) =>
            await this.client.Cypher
                .Match("(blog:Blog)-[inR:CONTAINS]->(comment:Comment)-[outR:IS_WRITTEN_BY]->(author:User)")
                .Where((Models.Blog.Blog blog) => blog.Id == blogId)
                .Delete("inR, outR, comment")
                .ExecuteWithoutResultsAsync();
        #endregion Helper Method(s)

        #region External Helper Method(s)
        public async Task DeleteBlogsByAuthorAsync(string username) =>
            await this.client.Cypher.Match("(author:User)-[inR:HAS_PUBLISHED]->(blog:Blog)")
                .Where((Models.User author) => author.UserName == username)
                .OptionalMatch("(blog)-[outR1:IS_RELEVANT_TO]->(topic:Topic)")
                .OptionalMatch("(blog)-[outR2:CONTAINS]->(comment:Comment)")
                .OptionalMatch("(comment)-[outR3:IS_WRITTEN_BY]->(commentAuthor:User)")
                .Delete("inR, outR1, outR2, outR3, comment, blog")
                .ExecuteWithoutResultsAsync();

        public async Task DeleteCommentsByAuthorAsync(string username) =>
            await this.client.Cypher.Match("(comment:Comment)-[outR:IS_WRITTEN_BY]->(author:User)")
                .Where((Models.User author) => author.UserName == username)
                .Match("(blog:Blog)-[inR:CONTAINS]->(comment)")
                .Delete("inR, outR, comment")
                .ExecuteWithoutResultsAsync();
        #endregion External Helper Method(s)
    }
}
